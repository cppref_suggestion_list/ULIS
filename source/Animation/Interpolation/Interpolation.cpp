// IDDN.FR.001.250001.005.S.P.2019.000.00000
// ULIS is subject to copyright laws and is the legal and intellectual property of Praxinos,Inc
/*
*   ULIS
*__________________
* @file         Interpolation.cpp
* @author       Thomas Schmitt
* @brief        This file provides implementation for interpolation between keys
* @license      Please refer to LICENSE.md
*/
#include "Animation/Interpolation/Interpolation.h"


